import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../service/library.service';
import { Endpoints } from '../endpoints';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.css']
})
export class AddressComponent implements OnInit {

  addresses: any;
  idToSearch: string;
  disabled: string;
  addressForm: any;

  constructor(private libraryService: LibraryService) {
    this.idToSearch = '';
    this.disabled = 'disabled';
    this.addressForm = {
      id : -1,
      postCode : '',
      city : '',
      street : '',
      streetNum : ''
    };
  }

  ngOnInit() {
    this.refreshPageData();
  }

  searchById(id: string) {
    if (id !== '') {
      this.libraryService.searchById(Endpoints.addressUrl, id).subscribe(data => {
        this.addresses = [data];
      });
    }
  }

  submitAddress() {
    if (this.addressForm.id === -1) {
      this.libraryService.add(Endpoints.addressUrl, this.addressForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    } else {
      this.libraryService.update(Endpoints.addressUrl, this.addressForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    }
  }

  removeAddress(address: any) {
    this.libraryService.remove(Endpoints.addressUrl, address.id).subscribe(() => this.refreshPageData());
  }

  refreshPageData() {
    this.libraryService.getAll(Endpoints.addressUrl).subscribe(data => {
      this.addresses = data;
    });
  }

  clearSearch() {
    this.idToSearch = '';
    this.refreshPageData();
  }

  editAddress(address: any) {
    this.addressForm.postCode = address.postCode;
    this.addressForm.city = address.city;
    this.addressForm.street = address.street;
    this.addressForm.streetNum = address.streetNum;
    this.addressForm.id = address.id;

    this.disabled = '';
  }

  clearForm() {
    this.addressForm.postCode = '';
    this.addressForm.city = '';
    this.addressForm.street = '';
    this.addressForm.streetNum = '';
    this.addressForm.id = -1;

    this.disabled = 'disabled';
  }
}
