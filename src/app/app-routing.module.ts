import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthorComponent } from './author/author.component';
import { AddressComponent } from './address/address.component';
import { MemberComponent } from './member/member.component';
import { LibraryComponent } from './library/library.component';
import { BookComponent } from './book/book.component';

const routes: Routes = [
  { path: '', component: AddressComponent },
  { path: 'addresses', component: AddressComponent },
  { path: 'authors', component: AuthorComponent },
  { path: 'books', component: BookComponent },
  { path: 'libraries', component: LibraryComponent },
  { path: 'members', component: MemberComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
