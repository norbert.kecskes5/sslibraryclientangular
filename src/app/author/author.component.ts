import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../service/library.service';
import { Endpoints } from '../endpoints';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css'],
})
export class AuthorComponent implements OnInit {

  authors: any;
  idToSearch: string;
  disabled: string;
  authorForm: any;

  constructor(private libraryService: LibraryService) {
    this.idToSearch = '';
    this.disabled = 'disabled';
    this.authorForm = {
      id : -1,
      firstName : '',
      lastName : ''
    };
  }

  ngOnInit() {
    this.refreshPageData();
  }

  searchById(id: string) {
    if (id !== '') {
      this.libraryService.searchById(Endpoints.authorUrl, id).subscribe(data => {
        this.authors = [data];
      });
    }
  }

  submitAuthor() {
    if (this.authorForm.id === -1) {
      this.libraryService.add(Endpoints.authorUrl, this.authorForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    } else {
      this.libraryService.update(Endpoints.authorUrl, this.authorForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    }
  }

  removeAuthor(author: any) {
    this.libraryService.remove(Endpoints.authorUrl, author.id).subscribe(() => this.refreshPageData());
  }

  refreshPageData() {
    this.libraryService.getAll(Endpoints.authorUrl).subscribe(data => {
      this.authors = data;
    });
  }

  clearSearch() {
    this.idToSearch = '';
    this.refreshPageData();
  }

  editAuthor(author: any) {
    this.authorForm.firstName = author.firstName;
    this.authorForm.lastName = author.lastName;
    this.authorForm.id = author.id;

    this.disabled = '';
  }

  clearForm() {
    this.authorForm.firstName = '';
    this.authorForm.lastName = '';
    this.authorForm.id = -1;

    this.disabled = 'disabled';
  }
}
