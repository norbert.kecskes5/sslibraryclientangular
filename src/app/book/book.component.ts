import { Component, OnInit } from '@angular/core';
import { LibraryService } from '../service/library.service';
import { Endpoints } from '../endpoints';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  books: any;
  idToSearch: string;
  disabled: string;
  bookForm: any;

  constructor(
    private libraryService: LibraryService
  ) {
    this.idToSearch = '';
    this.disabled = 'disabled';
    this.bookForm = {
      id : -1,
      title : '',
      year : '',
      language : '',
      authorId : '',
      libraryId : '',
      status : ''
    };
  }

  ngOnInit() {
    this.refreshPageData();
  }

  searchById(id: string) {
    if (id !== '') {
      this.libraryService.searchById(Endpoints.bookUrl, id).subscribe(data => {
        this.books = [data];
      });
    }
  }

  submitBook() {
    if (this.bookForm.id === -1) {
      this.libraryService.add(Endpoints.bookUrl, this.bookForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    } else {
      this.libraryService.update(Endpoints.bookUrl, this.bookForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    }
  }

  switchBookStatus(book: any) {
    book.status = book.status === 'TAKEN' ? 'IN_PLACE' : 'TAKEN';

    this.libraryService.update(Endpoints.bookUrl, book).subscribe(() => {
      this.refreshPageData();
      this.clearForm();
    });
  }

  removeBook(book: any) {
    this.libraryService.remove(Endpoints.bookUrl, book.id).subscribe(() => this.refreshPageData());
  }

  refreshPageData() {
    this.libraryService.getAll(Endpoints.bookUrl).subscribe(data => {
      this.books = data;

      this.books.forEach(book => {
        this.libraryService.searchById(Endpoints.authorUrl, book.authorId).subscribe(authorData => {
          book.author = this.convertAuthor(authorData);
        });
      });

      this.books.forEach(book => {
        this.libraryService.searchById(Endpoints.libraryUrl, book.libraryId).subscribe(libraryData => {
          book.library = this.convertLibrary(libraryData);
        });
      });
    });
  }

  convertAuthor(author: any) {
    return author.firstName + ' ' + author.lastName;
  }

  convertLibrary(author: any) {
    return author.name;
  }

  clearSearch() {
    this.idToSearch = '';
    this.refreshPageData();
  }

  editBook(book: any) {
    this.bookForm.title = book.title;
    this.bookForm.year = book.year;
    this.bookForm.language = book.language;
    this.bookForm.authorId = book.authorId;
    this.bookForm.libraryId = book.libraryId;
    this.bookForm.status = book.status;
    this.bookForm.id = book.id;

    this.disabled = '';
  }

  clearForm() {
    this.bookForm.title = '';
    this.bookForm.year = '';
    this.bookForm.language = '';
    this.bookForm.authorId = '';
    this.bookForm.libraryId = '';
    this.bookForm.status = '';
    this.bookForm.id = -1;

    this.disabled = 'disabled';
  }
}
