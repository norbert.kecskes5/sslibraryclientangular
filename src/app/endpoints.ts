export class Endpoints {

  public static addressUrl = 'http://localhost:8080/SsLibrary/address/';

  public static authorUrl = 'http://localhost:8080/SsLibrary/author/';

  public static bookUrl = 'http://localhost:8080/SsLibrary/book/';

  public static libraryUrl = 'http://localhost:8080/SsLibrary/library/';

  public static memberUrl = 'http://localhost:8080/SsLibrary/member/';
}
