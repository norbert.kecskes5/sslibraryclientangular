import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LibraryService } from '../service/library.service';
import { Endpoints } from '../endpoints';

@Component({
  selector: 'app-library',
  templateUrl: './library.component.html',
  styleUrls: ['./library.component.css']
})
export class LibraryComponent implements OnInit {

  libraries: any;
  idToSearch: string;
  disabled: string;
  libraryForm: any;

  constructor(
    private libraryService: LibraryService
  ) {
    this.idToSearch = '';
    this.disabled = 'disabled';
    this.libraryForm = {
      id : -1,
      name : '',
      addressId : ''
    };
  }

  ngOnInit() {
    this.refreshPageData();
  }

  searchById(id: string) {
    if (id !== '') {
      this.libraryService.searchById(Endpoints.libraryUrl, id).subscribe(data => {
        this.libraries = [data];
      });
    }
  }

  submitLibrary() {
    if (this.libraryForm.id === -1) {
      this.libraryService.add(Endpoints.libraryUrl, this.libraryForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    } else {
      this.libraryService.update(Endpoints.libraryUrl, this.libraryForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    }
  }

  removeLibrary(library: any) {
    this.libraryService.remove(Endpoints.libraryUrl, library.id).subscribe(() => this.refreshPageData());
  }

  refreshPageData() {
    this.libraryService.getAll(Endpoints.libraryUrl).subscribe(data => {
      this.libraries = data;

      this.libraries.forEach(library => {
        this.libraryService.searchById(Endpoints.addressUrl, library.addressId).subscribe(addressData => {
          library.address = this.convertAddress(addressData);
        });
      });
    });
  }

  convertAddress(address: any) {
    return address.postCode + ' ' + address.city + ' ' + address.street + ' ' + address.streetNum;
  }

  clearSearch() {
    this.idToSearch = '';
    this.refreshPageData();
  }

  editLibrary(library: any) {
    this.libraryForm.name = library.name;
    this.libraryForm.addressId = library.addressId;
    this.libraryForm.id = library.id;

    this.disabled = '';
  }

  clearForm() {
    this.libraryForm.name = '';
    this.libraryForm.addressId = '';
    this.libraryForm.id = -1;

    this.disabled = 'disabled';
  }
}
