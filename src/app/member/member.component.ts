import { Component, OnInit } from '@angular/core';
import { LibraryService } from '../service/library.service';
import { Endpoints } from '../endpoints';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  members: any;
  idToSearch: string;
  disabled: string;
  readonly: string;
  memberForm: any;

  constructor(
    private libraryService: LibraryService
  ) {
    this.idToSearch = '';
    this.disabled = 'disabled';
    this.readonly = '';
    this.memberForm = {
      id : -1,
      firstName : '',
      lastName : '',
      age : '',
      role : '',
      addressId : '',
      libraryId : ''
    };
  }

  ngOnInit() {
    this.refreshPageData();
  }

  searchById(id: string) {
    if (id !== '') {
      this.libraryService.searchById(Endpoints.memberUrl, id).subscribe(data => {
        this.members = [data];
      });
    }
  }

  submitMember() {
    if (this.memberForm.id === -1) {
      this.libraryService.add(Endpoints.memberUrl, this.memberForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    } else {
      this.libraryService.update(Endpoints.memberUrl, this.memberForm).subscribe(() => {
        this.refreshPageData();
        this.clearForm();
      });
    }
  }

  removeMember(member: any) {
    this.libraryService.remove(Endpoints.memberUrl, member.id).subscribe(() => this.refreshPageData());
  }

  refreshPageData() {
    this.libraryService.getAll(Endpoints.memberUrl).subscribe(data => {
      this.members = data;

      this.members.forEach(member => {
        this.libraryService.searchById(Endpoints.addressUrl, member.addressId).subscribe(addressData => {
          member.address = this.convertAddress(addressData);
        });
      });

      this.members.forEach(member => {
        this.libraryService.searchById(Endpoints.libraryUrl, member.libraryId).subscribe(libraryData => {
          member.library = this.convertLibrary(libraryData);
        });
      });
    });
  }

  convertAddress(address: any) {
    return address.postCode + ' ' + address.city + ' ' + address.street + ' ' + address.streetNum;
  }

  convertLibrary(author: any) {
    return author.name;
  }

  clearSearch() {
    this.idToSearch = '';
    this.refreshPageData();
  }

  editMember(member: any) {
    this.memberForm.firstName = member.firstName;
    this.memberForm.lastName = member.lastName;
    this.memberForm.age = member.age;
    this.memberForm.role = member.role;
    this.memberForm.addressId = member.addressId;
    this.memberForm.libraryId = member.libraryId;
    this.memberForm.id = member.id;

    this.disabled = '';
    this.readonly = 'readonly';
  }

  clearForm() {
    this.memberForm.firstName = '';
    this.memberForm.lastName = '';
    this.memberForm.age = '';
    this.memberForm.role = '';
    this.memberForm.addressId = '';
    this.memberForm.libraryId = '';
    this.memberForm.id = -1;

    this.disabled = 'disabled';
    this.readonly = '';
  }
}
