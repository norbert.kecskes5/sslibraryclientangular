import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  headers: any;

  constructor(private http: HttpClient) {
    this.headers = new Headers({
      'Content-Type': 'application/json'
    });
  }

  getAll(url: string) {
    return this.http.get(url)
      .pipe(catchError(this.errorHandler));
  }

  searchById(url: string, id: string) {
    return this.http.get(url + id)
      .pipe(catchError(this.errorHandler));
  }

  remove(url: string, id: string) {
    return this.http.delete(url + id, this.headers)
      .pipe(catchError(this.errorHandler));
  }

  add(url: string, form: any) {
    return this.http.post(url, form, this.headers)
      .pipe(catchError(this.errorHandler));
  }

  update(url: string, form: any) {
    return this.http.put(url + form.id, form, this.headers)
      .pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    console.log('Error happened: ' + error.message);

    return observableThrowError(error.message || 'Server Error!');
  }
}
